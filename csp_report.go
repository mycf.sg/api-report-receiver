package main

// CSPReportBody represents the structure of the CSP Report post
type CSPReportBody struct {
	CSPReport CSPReport `json:"csp-report"`
}

// CSPReport represents the structure of the CSP report
type CSPReport struct {
	BlockedURI         string `json:"blocked-uri"`
	ColumnNumber       uint   `json:"column-number"`
	Disposition        string `json:"disposition"`
	DocumentURI        string `json:"document-uri"`
	EffectiveDirective string `json:"effective-directive"`
	LineNumber         uint   `json:"line-number"`
	OriginalPolicy     string `json:"original-policy"`
	Referrer           string `json:"referrer"`
	ScriptSample       string `json:"script-sample"`
	SourceFile         string `json:"source-file"`
	StatusCode         uint   `json:"status-code"`
	ViolatedDirective  string `json:"violated-directive"`
}

// Log does the CSP report via the provided Logger
func (cspReport *CSPReport) Log(log Logger) {
	log.WithFields(cspReport.toMapStringInterface()).Info("csp-report")
}

// toMapStringInterface converts the data structure into the logged version
func (cspReport *CSPReport) toMapStringInterface() map[string]interface{} {
	return map[string]interface{}{
		"blockedUri":         cspReport.BlockedURI,
		"columnNumber":       cspReport.ColumnNumber,
		"disposition":        cspReport.Disposition,
		"documentUri":        cspReport.DocumentURI,
		"effectiveDirective": cspReport.EffectiveDirective,
		"lineNumber":         cspReport.LineNumber,
		"originalPolicy":     cspReport.OriginalPolicy,
		"referrer":           cspReport.Referrer,
		"scriptSample":       cspReport.ScriptSample,
		"sourceFile":         cspReport.SourceFile,
		"statusCode":         cspReport.StatusCode,
		"violatedDirective":  cspReport.ViolatedDirective,
	}
}

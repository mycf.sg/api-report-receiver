package main

import (
	"github.com/spf13/viper"
)

var (
	// config is the global configuration class
	config = NewConfig()
)

const (
	// ModeProduction defines a symbol for denoting running in production
	ModeProduction = "production"
	// ModeDevelopment defines a symbol for denoting running in development
	ModeDevelopment = "development"
)

const (
	// DefaultAddr defines the default listening addr
	DefaultAddr = "0.0.0.0"
	// DefaultMode defines the default mode to run in (ModeProduction/ModeDevelopment)
	DefaultMode = ModeProduction
	// DefaultPort defines the default port to listen on
	DefaultPort = "22287"
	// DefaultCSPURI defines the default URI to listen to CSP reports
	DefaultCSPURI = "/csp"
	// DefaultXSSURI defines the default URI to listen to XSS reports
	DefaultXSSURI = "/xss"
	// DefaultLogPretty defines the default setting for pretty printing in production
	DefaultLogPretty = false
	// DefaultLogCaller defines the default setting for including the caller function in logs
	DefaultLogCaller = false
	// DefaultFluentdHost defines the default FluentD host
	DefaultFluentdHost = "0.0.0.0"
	// DefaultFluentdPort defines the default FluentD port
	DefaultFluentdPort = 24224
	// DefaultFluentdTag defines the default FluentD tag
	DefaultFluentdTag = "report_receiver"
	// DefaultFluentdConnectionRetries defines the default number of times we'll try to make contact with the FluentD service
	DefaultFluentdConnectionRetries = 10
)

// Config holds the configuration values
type Config struct {
	Addr                     string
	Mode                     string
	Port                     string
	LogCaller                bool
	LogPretty                bool
	CSPURI                   string
	XSSURI                   string
	FluentdHost              string
	FluentdPort              int
	FluentdTag               string
	FluentdConnectionRetries int
}

// NewConfig is a convenience function for creating a new configuration struct
func NewConfig() *Config {
	return &Config{
		Addr:                     stringFromEnvironment("ADDR", DefaultAddr),
		Mode:                     stringFromEnvironment("MODE", DefaultMode),
		Port:                     stringFromEnvironment("PORT", DefaultPort),
		LogCaller:                boolFromEnvironment("LOG_CALLER", DefaultLogCaller),
		LogPretty:                boolFromEnvironment("LOG_PRETTY", DefaultLogPretty),
		CSPURI:                   stringFromEnvironment("CSP_URI", DefaultCSPURI),
		XSSURI:                   stringFromEnvironment("XSS_URI", DefaultXSSURI),
		FluentdHost:              stringFromEnvironment("FLUENTD_HOST", DefaultFluentdHost),
		FluentdPort:              intFromEnvironment("FLUENTD_PORT", DefaultFluentdPort),
		FluentdTag:               stringFromEnvironment("FLUENTD_TAG", DefaultFluentdTag),
		FluentdConnectionRetries: intFromEnvironment("FLUENTD_CONNECTION_RETRIES", DefaultFluentdConnectionRetries),
	}
}

// intFromEnvironment retrieves an integer key from the environment
func intFromEnvironment(key string, defaultValue int) int {
	viper.SetDefault(key, defaultValue)
	viper.BindEnv(key)
	return viper.GetInt(key)
}

// stringFromEnvironment retrieves a string key from the environment
func stringFromEnvironment(key, defaultValue string) string {
	viper.SetDefault(key, defaultValue)
	viper.BindEnv(key)
	return viper.GetString(key)
}

// boolFromEnvironment retrieves a bool key from the environment
func boolFromEnvironment(key string, defaultValue bool) bool {
	viper.SetDefault(key, defaultValue)
	viper.BindEnv(key)
	return viper.GetBool(key)
}

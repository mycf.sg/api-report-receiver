//go:generate go run ./generators/versioning/main.go
package main

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	globalLogger := NewLogger()
	globalLogger.ReportCaller = config.LogCaller
	log = globalLogger

	log.Infof("hello there! application is running in '%s'", config.Mode)

	log.Info("setting up remote logging connection...")
	go setupFluentdConnection(log, config.FluentdHost, config.FluentdPort, config.FluentdTag, config.FluentdConnectionRetries)

	log.Info("setting up logical endpoint handlers...")
	mux := http.NewServeMux()
	mux.Handle(config.CSPURI,
		prometheus.InstrumentHandler(
			config.CSPURI,
			http.HandlerFunc(handleCSPReport),
		),
	)
	mux.Handle(config.XSSURI,
		prometheus.InstrumentHandler(
			config.XSSURI,
			http.HandlerFunc(handleXSSReport),
		),
	)

	log.Info("setting up peripheral endpoint handlers...")
	mux.HandleFunc("/", handleLivenessCheck)
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/healthz", handleLivenessCheck)
	mux.HandleFunc("/readyz", handleLivenessCheck)

	listenInterface := config.Addr + ":" + config.Port
	httpListen(listenInterface, mux)
}

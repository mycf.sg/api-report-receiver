# this makefile holds common development operations
BIN_NAME=api-report-receiver
BIN_PATH=bin

dev: setup
	@godev --env MODE=development
start: setup
	@godev
deps:
	@go mod vendor
generate_version:
	@go generate
build:
	@$(MAKE) build_linux
	@cp bin/$(BIN_NAME)-linux-amd64 bin/$(BIN_NAME)-v$$(docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q)-linux-amd64
	@$(MAKE) build_macos
	@cp bin/$(BIN_NAME)-darwin-amd64 bin/$(BIN_NAME)-v$$(docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q)-darwin-amd64
	@$(MAKE) build_windows
	@cp bin/$(BIN_NAME)-windows-386.exe bin/$(BIN_NAME)-v$$(docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q)-windows-386.exe
build_windows: generate_version
	@GOOS=windows GOARCH=386 CGO_ENABLED=0 GO111MODULE=on \
		go build -a -ldflags '-w -extldflags "-static"' -o bin/$(BIN_NAME)-windows-386.exe
build_macos: generate_version
	@GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 GO111MODULE=on \
		go build -a -ldflags '-w -extldflags "-static"' -o bin/$(BIN_NAME)-darwin-amd64
	@chmod +x bin/$(BIN_NAME)-darwin-amd64
build_linux: generate_version
	@GOOS=linux GOARCH=amd64 CGO_ENABLED=0 GO111MODULE=on \
		go build -a -ldflags '-w -extldflags "-static"' -o bin/$(BIN_NAME)-linux-amd64
	@chmod +x bin/$(BIN_NAME)-linux-amd64
deploy_helm:
	@helm install --name api-report-receiver ./run/api-report-receiver
deploy_helm_d:
	@helm delete api-report-receiver
image:
	@docker build \
		--build-arg BIN_NAME=$(BIN_NAME) \
		--tag mycfsg/api-report-receiver:latest \
		.
image_tag: image
	@docker tag mycfsg/api-report-receiver:latest mycfsg/api-report-receiver:v$$(docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q)
image_push: image_tag
	@docker push mycfsg/api-report-receiver:latest
	@docker push mycfsg/api-report-receiver:v$$(docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q)
k8s:
	@helm template --name api-report-receiver ./run/api-report-receiver > ./run/deployme.yaml
k8s_deploy: k8s
	@kubectl apply -f ./run/deployme.yaml
version_latest:
	@docker run -v "$$(pwd):/app" govtechsg/semver:latest get-latest -q
version_bump:
	@docker run -v "$$(pwd):/app" govtechsg/semver:latest iterate ${VERSION} -i
setup:
	@docker-compose up -d -V
fix_permissions:
	@sudo chmod $$(whoami):$$(whoami) -R .
show_fluentd:
	@docker logs -f $$(docker ps | grep fluentd | cut -f 1 -d ' ')
show_all_logs:
	@sleep 5
	@docker-compose logs -f
test:
	go test ./... -coverprofile c.out
test_watch:
	godev test
package main

import (
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/sirupsen/logrus"
)

var (
	// DefaultHookLevels defines default levels for which
	DefaultHookLevels = []logrus.Level{
		logrus.TraceLevel,
		logrus.DebugLevel,
		logrus.InfoLevel,
		logrus.WarnLevel,
		logrus.ErrorLevel,
		logrus.FatalLevel,
		logrus.PanicLevel,
	}
)

// FluentHook to send logs via fluentd.
type FluentHook struct {
	instance *fluent.Fluent
	levels   []logrus.Level
	tag      string
}

// NewFluentHook creates a new hook to send to fluentd.
func NewFluentHook(config fluent.Config) (*FluentHook, error) {
	var logger *fluent.Fluent
	var err error
	if logger, err = fluent.New(config); err != nil {
		return nil, err
	}
	return &FluentHook{
		instance: logger,
		levels:   DefaultHookLevels,
		tag:      "app",
	}, err
}

// Fire sends the logs
func (fluentHook *FluentHook) Fire(entry *logrus.Entry) error {
	msg := fluentHook.buildMessage(entry)
	tag := fluentHook.tag
	if tagProperty, isPresent := entry.Data["tag"]; isPresent {
		tag = fmt.Sprint(tagProperty)
	}
	fluentHook.instance.Post(tag, msg)
	return nil
}

// AddLevels adds levels for which logs should be sent to FluentD
func (fluentHook *FluentHook) AddLevels(levels ...logrus.Level) {
	for level := 0; level < len(levels); level++ {
		fluentHook.levels = append(fluentHook.levels, levels[level])
	}
}

// Levels returns the log levels for which logs should be sent to FluentD
func (fluentHook *FluentHook) Levels() []logrus.Level {
	return fluentHook.levels
}

// buildMessage creates the message structure for FluentD
func (fluentHook *FluentHook) buildMessage(entry *logrus.Entry) map[string]interface{} {
	data := make(map[string]interface{})

	for k, v := range entry.Data {
		if k == "tag" {
			continue
		}
		data[k] = v
	}
	data["msg"] = entry.Message
	data["level"] = entry.Level.String()

	return data
}

# Report Receiver
This is a client-side violation report receiver that pipes the received reports to centralised logging.

# Usage

## Supported Loggers

- [x] FluentD

## As Binary
The binary is released on the releases page.

## As Docker Image
The Docker image is released at https://hub.docker.com/r/mycfsg/api-report-receiver.

To use it:

```yaml
version: "3.5"
services:
  # ...
  report_receiver:
    image: mycfsg/api-report-receiver:latest
```

## From Helm Chart
The Helm chart is stored at [run/api-report-receiver](./run/api-report-receiver).

To install it:

```sh
make deploy_helm
# OR
helm install --name api-report-receiver ./run/api-report-receiver
```

# Configuration

| Key | Description |
| `ADDR` | The interface address to bind to to listen for requests |
| `MODE` | `"development"` or `"production"` |
| `PORT` | The interface port to bind to to listen for requests |
| `LOG_CALLER` | When set to true, includes code reference in logs |
| `LOG_PRETTY` | When set to true, pretty prints JSON |
| `CSP_URI` | HTTP request path for submitting CSP reports |
| `XSS_URI` | HTTP request path for submitting XSS reports |
| `FLUENTD_HOST` | The FluentD service hostname |
| `FLUENTD_PORT` | The FluentD service port |
| `FLUENTD_TAG` | The tag used when sending data to FluentD |
| `FLUENTD_CONNECTION_RETRIES` | Number of retries connecting to FluentD before dying |

# License
This project is licensed under the MIT license. See [LICENSE] file for full text.

# Cheers

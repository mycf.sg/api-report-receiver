module app

require (
	github.com/fluent/fluent-logger-golang v1.4.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/sirupsen/logrus v1.4.1
	github.com/spf13/viper v1.3.2
	github.com/tinylib/msgp v1.1.0 // indirect
)

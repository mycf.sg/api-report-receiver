package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// handleCSPReport handles CSP violations
func handleCSPReport(w http.ResponseWriter, r *http.Request) {
	defer handleUnexpectedError(w, r)
	var cspReportBody CSPReportBody
	rawBody := readHTTPRequestBody(r)
	if len(rawBody) == 0 {
		w.WriteHeader(400)
		w.Write(NewResponse("ERROR_EMPTY_BODY", "request body was empty when it shouldn't have been").Bytes())
	} else if unmarshalError := json.Unmarshal(rawBody, &cspReportBody); unmarshalError != nil {
		handleJSONError(unmarshalError, w, r)
	} else {
		cspReportBody.CSPReport.Log(log)
		handleSuccess(string(rawBody), w, r)
	}
}

// handleXSSReport hanldes XSS violations
func handleXSSReport(w http.ResponseWriter, r *http.Request) {
	defer handleUnexpectedError(w, r)
	var xssReportBody XSSReportBody
	rawBody := readHTTPRequestBody(r)
	if len(rawBody) == 0 {
		w.WriteHeader(400)
		w.Write(NewResponse("ERROR_EMPTY_BODY", "request body was empty when it shouldn't have been").Bytes())
	} else if unmarshalError := json.Unmarshal(rawBody, &xssReportBody); unmarshalError != nil {
		handleJSONError(unmarshalError, w, r)
	} else {
		xssReportBody.XSSReport.Log(log)
		handleSuccess(string(rawBody), w, r)
	}
}

// handleReadinessCheck checks for readiness and responds with 200 if so, 500 otherwise
func handleReadinessCheck(w http.ResponseWriter, r *http.Request) {
	defer handleUnexpectedError(w, r)
	if fluentdConnectionReady {
		handleLivenessCheck(w, r)
	} else {
		w.WriteHeader(500)
		w.Write(NewResponse("NOT_READY", "not yet").Bytes())
	}
}

// handleLivenessCheck is a generic 200 responder
func handleLivenessCheck(w http.ResponseWriter, r *http.Request) {
	defer handleUnexpectedError(w, r)
	response := NewResponse("OK", "ok")
	w.WriteHeader(200)
	w.Write(response.Bytes())
}

func handleJSONError(err error, w http.ResponseWriter, r *http.Request) {
	log.Error(err)
	response := NewResponse("ERROR_JSON", err.Error())
	w.WriteHeader(400)
	w.Write(response.Bytes())
}

func handleSuccess(body string, w http.ResponseWriter, r *http.Request) {
	response := NewResponse("OK_CSP", body)
	w.WriteHeader(200)
	w.Write(response.Bytes())
}

// handleUnexpectedError should handle any error possibly thrown
func handleUnexpectedError(w http.ResponseWriter, r *http.Request) {
	if err := recover(); err != nil {
		log.Warn(err)
		w.WriteHeader(500)
		defer func() {
			if r := recover(); r != nil {
				log.Error(r)
				response := NewResponse("ERROR_UNKNOWN", fmt.Sprint(r))
				w.Write(response.Bytes())
			}
		}()
		if err, isError := err.(error); isError {
			response := NewResponse("ERROR", err.Error())
			w.Write(response.Bytes())
		} else {
			w.Write([]byte(fmt.Sprintf("%s", err)))
		}
	}
}

package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

// Logger defines the logger type for this service
type Logger interface {
	Tracef(string, ...interface{})
	Trace(...interface{})
	Debugf(string, ...interface{})
	Debug(...interface{})
	Infof(string, ...interface{})
	Info(...interface{})
	Warnf(string, ...interface{})
	Warn(...interface{})
	Errorf(string, ...interface{})
	Error(...interface{})
	Panicf(string, ...interface{})
	Panic(...interface{})
	Fatalf(string, ...interface{})
	Fatal(...interface{})
	WithFields(logrus.Fields) *logrus.Entry
	AddHook(logrus.Hook)
}

var log Logger

// NewLogger creates a new logger
func NewLogger() *logrus.Logger {
	return &logrus.Logger{
		Formatter: &logrus.TextFormatter{
			DisableSorting:   true,
			ForceColors:      true,
			FullTimestamp:    true,
			QuoteEmptyFields: true,
			TimestampFormat:  "2006-01-02 15:04:05",
		},
		Hooks:        make(logrus.LevelHooks),
		Level:        logrus.TraceLevel,
		Out:          os.Stdout,
		ReportCaller: true,
	}
}

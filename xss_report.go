package main

// XSSReportBody represents the literal body structure of the XSS report
type XSSReportBody struct {
	XSSReport `json:"xss-report"`
}

// XSSReport represents the XSS report itself
type XSSReport struct {
	RequestURL  string `json:"request-url"`
	RequestBody string `json:"request-body"`
}

// Log sends an xss log to the server
func (xssReport *XSSReport) Log(log Logger) {
	log.WithFields(xssReport.toMapStringInterface()).Info("xss-report")
}

// toMapStringInterface converts this struct into a map string interface{}
func (xssReport *XSSReport) toMapStringInterface() map[string]interface{} {
	return map[string]interface{}{
		"requestBody": xssReport.RequestBody,
		"requestUrl":  xssReport.RequestURL,
	}
}

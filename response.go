package main

import "encoding/json"

// NewResponse is a convenience function for creating responses
func NewResponse(code, message string) *Response {
	return &Response{Code: code, Message: message}
}

// Response defines the data schema for all responses by this service
type Response struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// String gets the string representation of this instance
func (response *Response) String() string {
	return string(response.Bytes())
}

// Bytes gets the []byte representation of this instance
func (response *Response) Bytes() []byte {
	var jsonResponse []byte
	var err error
	if jsonResponse, err = json.Marshal(response); err != nil {
		panic(err)
	}
	return jsonResponse
}

package main

import (
	"io/ioutil"
	"net/http"
	"time"

	"github.com/fluent/fluent-logger-golang/fluent"
)

var fluentdConnectionReady = false

// readHTTPRequestBody processes the request's body and returns the body data
func readHTTPRequestBody(r *http.Request) []byte {
	body, readBodyError := ioutil.ReadAll(r.Body)
	if readBodyError != nil {
		log.Error(readBodyError)
		panic(readBodyError)
	}
	return body
}

// setupFluentdConnection sets up a connection to the remote FluentD using the provided Logger
func setupFluentdConnection(log Logger, host string, port int, tag string, retryCount ...int) {
	hook, err := NewFluentHook(fluent.Config{
		FluentHost:   host,
		FluentPort:   port,
		RequestAck:   true,
		Timeout:      time.Second * 5,
		WriteTimeout: time.Second * 5,
		MaxRetry:     20,
	})

	// retry logic
	if err != nil {
		fluentdConnectionReady = false
		if len(retryCount) > 0 {
			if retryCount[0] <= 0 {
				log.Error(err)
				panic(err)
			} else {
				retriesLeft := retryCount[0] - 1
				log.Errorf("failed to establish connection to fluentd: '%s' [retries left: %v]", err, retriesLeft)
				time.Sleep(time.Second * 5)
				setupFluentdConnection(log, host, port, tag, retriesLeft)
			}
		} else {
			log.Errorf("failed to establish connection to fluentd: '%s' [retries left: 10]", err)
			time.Sleep(time.Second * 5)
			setupFluentdConnection(log, host, port, tag, 10)
		}
	} else {
		hook.tag = tag
		log.Infof("connection established to fluentd at '%s:%v'", host, port)
		log.AddHook(hook)
		fluentdConnectionReady = true
	}
}

// httpListen listens on :listenOn using the mux :app
func httpListen(listenOn string, app http.Handler) {
	log.Infof("attempting to listen on %s...", listenOn)
	listenError := http.ListenAndServe(listenOn, httpLog(app))
	if listenError != nil {
		log.Errorf("error occurred while attempting to listen: \"%s\"", listenError.Error())
	}
}

// httpLog is a middleware for logging HTTP requests
func httpLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		log.WithFields(map[string]interface{}{
			"host":           r.Host,
			"protocol":       r.Proto,
			"content_length": r.ContentLength,
			"method":         r.Method,
			"uri":            r.RequestURI,
			"url":            r.URL.Path,
			"remote_addr":    r.RemoteAddr,
			"duration_ms":    time.Since(start).Seconds() * 1000,
			"level":          "access",
		}).Trace("access")
	})
}
